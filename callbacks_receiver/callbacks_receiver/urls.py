"""callbacks_receiver URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf.urls.static import static
from django.urls import include
from django.urls import path
from rest_framework import permissions

from .settings import STATIC_ROOT
from .settings import STATIC_URL


schema_view = get_schema_view(
   openapi.Info(
      title='Callbacks Receiver API',
      default_version='',
      description='Сервис приёма и временного хранения коллбеков для тестирования асинхронных HTTP-запросов.',
   ),
   public=True,
   permission_classes=[permissions.AllowAny],
)


urlpatterns = [
    path('register_key/', include('key_register.urls'), name='register_key'),
    path('callback/', include('receiver.urls'), name='callbacks_endpoint'),
    path('ServiceModel/AuthService.svc/Login', include('authorization.urls'), name='fake_authorization'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
] + static(STATIC_URL, document_root=STATIC_ROOT)
