from django.urls import path

from .views import FakeAuth


urlpatterns = [
    path('', FakeAuth.as_view(), name='fake_authorization'),
]
