from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class FakeAuth(APIView):
    """
    Фейковая авторизация.
    """

    @swagger_auto_schema(
        operation_summary='Авторизация для сервиса sk-manager',
        operation_description='Производится имитация авторизации для сервиса sk-manager. Необходимо для корректного '
                              'получения callback-ов.',
        responses={
            200: 'Авторизация сервиса sk-manager прошла успешно',
        }
    )
    def post(self, request):
        """
        Авторизация сервиса sk-manager.

        :param request: Запрос на авторизацию от сервиса sk-manager.
        """
        print(request)
        return Response(status=status.HTTP_200_OK)
