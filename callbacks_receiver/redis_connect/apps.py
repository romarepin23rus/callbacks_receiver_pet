from django.apps import AppConfig


class RedisConnectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'redis_connect'
