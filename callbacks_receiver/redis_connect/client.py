from datetime import datetime
from hashlib import md5
from json import dumps
from json import loads
from typing import List
from typing import Optional

from redis import StrictRedis

from callbacks_receiver.settings import REDIS_DB
from callbacks_receiver.settings import REDIS_HOST
from callbacks_receiver.settings import REDIS_PORT


class RedisConnect:
    """
    Подключение к Redis.
    """
    def __init__(self):
        """
        Создание объекта подключения к Redis серверу на основе значений для подключения в настройках.
        """
        self._redis_connect = StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

    def initialize_record(self) -> str:
        """
        Добавление нового ключа с пустым значением.

        :return: Новый ключ.
        """
        now = str(datetime.now())
        new_key = md5(now.encode()).hexdigest()
        self._redis_connect.set(new_key, dumps(None))
        return new_key

    def write_value(self, key: str, value: dict) -> bool:
        """
        Запись значения по указанному ключу в Redis.

        :param key: Ранее зарегистрированный ключ.
        :param value: Записываемое значение.
        :return: True - запись успешно произведена, False - запись невозможна, так как значение соответствующее ключу
        не пустое или ключ отсутствует в списке.
        """
        has_recorded = False
        if key in self._get_keys():
            current_value = loads(self._redis_connect.get(key))
            if not current_value:
                self._redis_connect.set(key, dumps(value))
                has_recorded = True
        return has_recorded

    def extract_record(self, key: str) -> Optional[dict]:
        """
        Чтение значения по указанному ключу и удаление записи из Redis.

        :param key: Ранее зарегистрированный ключ.
        :return: Значение соответствующее указанному ключу.
        """
        data = self._redis_connect.get(key)
        if isinstance(data, bytes):
            data = loads(data)
            if data:
                self._redis_connect.delete(key)
        return data

    def _get_keys(self) -> List[str]:
        """
        Получение списка ключей Redis.

        :return: Список ключей.
        """
        keys = [key.decode() for key in self._redis_connect.keys()]
        return keys
