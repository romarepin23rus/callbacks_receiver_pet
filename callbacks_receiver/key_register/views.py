from drf_yasg.openapi import Schema
from drf_yasg.openapi import TYPE_OBJECT
from drf_yasg.openapi import TYPE_STRING
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from redis_connect import RedisConnect


response_schema = Schema(
    type=TYPE_OBJECT,
    properties={
        'key': Schema(
            type=TYPE_STRING,
            description='Ключ записи в Redis',
            example='d88678cc91ec25c63275edba64d7c9c8',
        )
    }
)


class KeyLogger(APIView):
    """
    Регистратор новых ключей.
    """

    @swagger_auto_schema(
        operation_summary='Регистрация ключа в Redis',
        operation_description='Новый уникальный ключ создаётся и  регистрируется в Redis',
        responses={
            201: response_schema,
        }
    )
    def get(self, request):
        """
        Регистрация нового ключа.

        :param request: Запрос на регистрацию нового ключа.
        """
        return Response(
            {
                'key': RedisConnect().initialize_record(),
            },
            status.HTTP_201_CREATED
        )
