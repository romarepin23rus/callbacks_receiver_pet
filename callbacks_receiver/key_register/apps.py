from django.apps import AppConfig


class KeyRegisterConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'key_register'
