from django.urls import path

from .views import KeyLogger


urlpatterns = [
    path('', KeyLogger.as_view(), name='register_key'),
]
