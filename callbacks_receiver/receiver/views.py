from json import loads

from drf_yasg.openapi import Schema
from drf_yasg.openapi import TYPE_BOOLEAN
from drf_yasg.openapi import TYPE_OBJECT
from drf_yasg.openapi import TYPE_STRING
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from redis_connect.client import RedisConnect


success_post_response_schema = Schema(
    type=TYPE_OBJECT,
    properties={
        'status': Schema(
            type=TYPE_BOOLEAN,
            title='Статус запроса',
        ),
        'message': Schema(
            type=TYPE_STRING,
            title='Сообщение от сервиса'
        ),
        'data': Schema(
            type=TYPE_OBJECT,
            title='Данные',
            description='В зависимости от запроса либо callback, либо None. Поля в теле callback-а могут отличаться.'
        ),
    },
    example={
        'status': True,
        'message': 'Callback successfully recorded by key d88678cc91ec25c63275edba64d7c9c8',
        'data': None,
    },
)
error_post_response_schema = Schema(
    type=TYPE_OBJECT,
    properties={
        'status': Schema(
            type=TYPE_BOOLEAN,
            title='Статус запроса',
        ),
        'message': Schema(
            type=TYPE_STRING,
            title='Сообщение от сервиса'
        ),
        'data': Schema(
            type=TYPE_OBJECT,
            title='Данные',
            description='В зависимости от запроса либо callback, либо None. Поля в теле callback-а могут отличаться.'
        ),
    },
    example={
        'status': False,
        'message': 'Record by key d88678cc91ec25c63275edba64d7c9c8 not registered or already available!',
        'data': None,
    },
)
success_get_response_schema = Schema(
    type=TYPE_OBJECT,
    properties={
        'status': Schema(
            type=TYPE_BOOLEAN,
            title='Статус запроса',
        ),
        'message': Schema(
            type=TYPE_STRING,
            title='Сообщение от сервиса'
        ),
        'data': Schema(
            type=TYPE_OBJECT,
            title='Данные',
            description='В зависимости от запроса либо callback, либо None. Поля в теле callback-а могут отличаться.'
        ),
    },
    example={
        'status': True,
        'message': 'Record by key d88678cc91ec25c63275edba64d7c9c8 successfully extracted.',
        'data': {
            'sk': 'INGOS',
            'status': True,
            'uuid': 'CL246966828',
            'form_id': '0f1b7b69-c61e-4986-92d3-e854aa4b0bdd',
            'summ': 13866.62,
            'errors': [],
        },
    },
)
error_get_response_schema = Schema(
    type=TYPE_OBJECT,
    properties={
        'status': Schema(
            type=TYPE_BOOLEAN,
            title='Статус запроса',
        ),
        'message': Schema(
            type=TYPE_STRING,
            title='Сообщение от сервиса'
        ),
        'data': Schema(
            type=TYPE_OBJECT,
            title='Данные',
            description='В зависимости от запроса либо callback, либо None. Поля в теле callback-а могут отличаться.'
        ),
    },
    example={
        'status': False,
        'message': 'Record by key d88678cc91ec25c63275edba64d7c9c8 not found!',
        'data': None,
    },
)


class SkManagerReceiver(APIView):
    """
    Приёмник callback-ов от сервиса sk-manager.
    """

    @swagger_auto_schema(
        operation_summary='Запись callback-а по заданному ключу',
        operation_description='Производится запись callback-а по зарегистрированному ранее ключу',
        responses={
            201: success_post_response_schema,
            400: error_post_response_schema,
        }
    )
    def post(self, request, key: str):
        """
        Запись callback-а по заданному ключу.

        :param request: Callback-запрос.
        :param key: Зарегистрированный ранее ключ ключ.
        """
        has_recorded = RedisConnect().write_value(key, loads(request.body))
        if has_recorded:
            response = {
                'status': True,
                'message': f'Callback successfully recorded by key {key}',
                'data': None,
            }
            status_code = status.HTTP_201_CREATED
        else:
            response = {
                'status': False,
                'message': f'Record by key {key} not registered or already available!',
                'data': None,
            }
            status_code = status.HTTP_400_BAD_REQUEST
        print(response.get('message'))
        return Response(response, status=status_code)

    @swagger_auto_schema(
        operation_summary='Извлечение callback-а по заданному ключу',
        operation_description='Производится извлечение callback-а по зарегистрированному ранее ключу и удаление записи '
                              'в Redis',
        responses={
            200: success_get_response_schema,
            400: error_get_response_schema,
        }
    )
    def get(self, request, key: str):
        """
        Извлечение callback-а по заданному ключу.

        :param request: Запрос на извлечение callback-а.
        :param key: Зарегистрированный ранее ключ ключ.
        """
        data = RedisConnect().extract_record(key)
        if data:
            response = {
                'status': True,
                'message': f'Record by key {key} successfully extracted.',
                'data': data,
            }
            status_code = status.HTTP_200_OK
        else:
            response = response = {
                'status': False,
                'message': f'Record by key {key} not found!',
                'data': None,
            }
            status_code = status.HTTP_400_BAD_REQUEST
        print(response.get('message'))
        return Response(response, status=status_code)
