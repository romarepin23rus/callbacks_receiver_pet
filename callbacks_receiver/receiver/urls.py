from django.urls import path

from .views import SkManagerReceiver


urlpatterns = [
    path('<slug:key>/', SkManagerReceiver.as_view(), name='callback'),
]
