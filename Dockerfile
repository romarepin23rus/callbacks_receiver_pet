FROM python:3.8.15-alpine3.15

RUN apk add git \
    python3-dev \
    g++ \
    libffi-dev

RUN git clone https://gitlab.com/romarepin23rus/callbacks_receiver_pet.git -b main
WORKDIR ./callbacks_receiver/callbacks_receiver
RUN pip install --upgrade pip \
    && pip install --no-cache-dir -r requirements.txt \
    && pip install --no-cache-dir uwsgi
RUN python manage.py collectstatic --noinput
RUN pip uninstall wheel -y
EXPOSE 8000
CMD ["uwsgi", "--http", ":8000", "--module", "callbacks_receiver.wsgi:application"]
